﻿using System.Collections;
using UnityEngine;

public class EngineControl : ShipComponent
{
    public float Throttle;
    public float EnginePower = 3f;
    private float actualThrottle;
    private float maxThrottle = 20f;

    private Rigidbody rb;
    private Light engineLight;

    public void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        engineLight = GetComponentInChildren<Light>();
    }

    public void FixedUpdate()
    {
        if (IsDestroyed) return;

        rb.AddForceAtPosition(-transform.forward*Throttle * EnginePower, transform.position, ForceMode.Force);
    }

    public void Update()
    {
        Throttle = Mathf.Clamp(Throttle, 0f, maxThrottle);
        actualThrottle = Mathf.Lerp(actualThrottle, Throttle, 0.1f);
        engineLight.intensity = IsDestroyed ? 0f : actualThrottle / 20f*8f;
    }

    public GameObject[] ExplosionEffects;

    protected override IEnumerator RunDestructionFx()
    {
        for (int i = 0; i < 3; i++)
        {
            var explosion = (GameObject)Instantiate(ExplosionEffects[Random.Range(0, ExplosionEffects.Length)], transform.position - (Vector3.one * 0.1f) + 0.2f * (Random.insideUnitSphere), Quaternion.identity);
            explosion.transform.localScale = Vector3.one * (0.1f + Random.value * 0.3f);
            yield return new WaitForSeconds(Random.value * 0.5f);
        }
    }
}
