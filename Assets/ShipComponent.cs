﻿using System.Collections;
using System.Linq;
using UnityEngine;

public abstract class ShipComponent : MonoBehaviour
{
    public bool IsDestroyed;
    public int Health;
    public int ComponentMass;

    public void Damage(int dmg)
    {
        if (IsDestroyed) return;
        
        Health -= dmg;
        if (Health <= 0)
        {
            DestroyShipComponent();
        }
    }

    protected abstract IEnumerator RunDestructionFx();

    private void DoPhysicsStuff()
    {
        var rb = gameObject.AddComponent<Rigidbody>();
        var compound = GetComponent<CompoundComponent>();
        var deltaMass = compound != null ? compound.CompoundMass : ComponentMass;

        rb.mass = deltaMass;
        rb.useGravity = false;

        var parentRb = transform.parent.GetComponentInParent<Rigidbody>();
        parentRb.mass -= deltaMass;

        var torque = Random.insideUnitSphere * 0.002f * ComponentMass;
        rb.AddTorque(torque, ForceMode.Impulse);
        parentRb.AddTorque(-torque, ForceMode.Impulse);

        var impulse = Random.insideUnitSphere * 0.002f * ComponentMass;
        rb.AddForce(impulse, ForceMode.Impulse);
        parentRb.AddForce(-impulse, ForceMode.Impulse);
        
        var ship = GetComponentInParent<Ship>();
        foreach (var cmp in ship.GetComponentsInChildren<CompoundComponent>())
        {
            cmp.RecalculateMass();
        }

        foreach (var shipPart in transform.GetComponentsInChildren<ShipComponent>().Skip(1))
        {
            if (Random.value > 0.8f)
            {
                shipPart.DestroyShipComponent();
            }
        }

        transform.SetParent(null);
    }

    public virtual void DestroyShipComponent()
    {
        IsDestroyed = true;

        DoPhysicsStuff();
        StartCoroutine(RunDestructionFx());
    }
}
