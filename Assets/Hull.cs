﻿using System.Collections;
using UnityEngine;

public class Hull : VitalShipComponent
{
    public GameObject[] ExplosionEffects;

    protected override IEnumerator RunDestructionFx()
    {
        for (int i = 0; i < 25; i++)
        {
            var explosion = (GameObject)Instantiate(ExplosionEffects[Random.Range(0, ExplosionEffects.Length)], transform.position - (Vector3.one * 0.1f) + 0.2f * (Random.insideUnitSphere), Quaternion.identity);
            explosion.transform.localScale = Vector3.one * (0.1f + Random.value * 0.3f);
            yield return new WaitForSeconds(Random.value * 0.5f);
        }
    }
}
