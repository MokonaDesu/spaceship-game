﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public int targetShipCount;

    private DragMouseOrbit orbiter;

    public GameObject ShipPrefab;

    public List<Ship> FirstGroupShips;

    public List<Ship> SecondGroupShips;

    public Ship PlayerShip;

    public void Start()
    {
        orbiter = FindObjectOfType<DragMouseOrbit>();
        StartCoroutine(GameChecker());
    }

    public void PickTargets()
    {
        foreach (var ship in FirstGroupShips)
        {
            if (!ship.HasTarget()) ship.PickRandomTarget(SecondGroupShips);
        }

        foreach (var ship in SecondGroupShips)
        {
            if (!ship.HasTarget()) ship.PickRandomTarget(FirstGroupShips);
        }
    }

    public IEnumerator GameChecker()
    {
        while (true)
        {
            var ships = FirstGroupShips.Union(SecondGroupShips).ToList();
            if (ships.Count < targetShipCount)
            {
                for (var i = 0; i < (targetShipCount - ships.Count)/2; i++)
                {
                    var ship = SpawnShip(new Vector3(0, 0, -40), Quaternion.Euler(0, -180, 0));

                    FirstGroupShips.Add(ship);
                }

                for (var i = 0; i < (targetShipCount - ships.Count)/2; i++)
                {
                    var ship = SpawnShip(new Vector3(0, 0, 40), Quaternion.identity);

                    SecondGroupShips.Add(ship);
                }
            }

            if (orbiter.target == null || orbiter.target.IsDestroyed)
            {
                PlayerShip = SpawnShip(new Vector3(-40, 0, 0), Quaternion.Euler(0, -90, 0));
                FirstGroupShips.Add(PlayerShip);
                orbiter.target = PlayerShip;
            }

            PickTargets();
            yield return new WaitForEndOfFrame();
            foreach (var ship in FirstGroupShips)
            {
                if (ship != PlayerShip) ship.ShipRb.AddForce(0, 0, 0.5f, ForceMode.VelocityChange);
                foreach (var gun in ship.GetComponentsInChildren<Gun>())
                {
                    gun.SetLaserColor(Color.red);
                }
            }
            foreach (var ship in SecondGroupShips)
            {
                ship.ShipRb.AddForce(0, 0, -0.5f, ForceMode.VelocityChange);
                foreach (var gun in ship.GetComponentsInChildren<Gun>())
                {
                    gun.SetLaserColor(Color.blue);
                }
            }
            yield return new WaitForSeconds(10f);
        }
    }

    public Ship SpawnShip(Vector3 origin, Quaternion rotation)
    {
        var obj = (GameObject)Instantiate(ShipPrefab, origin + Random.insideUnitSphere * 15, rotation);
        return obj.GetComponent<Ship>();
    }

    public void NotifyShipDestroyed(Ship s)
    {
        if (FirstGroupShips.Contains(s)) FirstGroupShips.Remove(s);
        if (SecondGroupShips.Contains(s)) SecondGroupShips.Remove(s);
    }
}
