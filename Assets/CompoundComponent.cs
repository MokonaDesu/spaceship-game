﻿using System.Linq;
using UnityEngine;

public class CompoundComponent : MonoBehaviour
{
    public int CompoundMass;

    public void RecalculateMass()
    {
        CompoundMass = GetComponentsInChildren<ShipComponent>().Sum(comp => comp.ComponentMass);
    }

    public virtual void Start()
    {
        RecalculateMass();
    }
}
