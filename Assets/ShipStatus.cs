﻿using UnityEngine;
using UnityEngine.UI;

public class ShipStatus : MonoBehaviour
{
    private DragMouseOrbit orbiter;
    private Text text;

    public void Start()
    {
        orbiter = FindObjectOfType<DragMouseOrbit>();
        text = GetComponent<Text>();
    }

	void Update ()
	{
	    var result = string.Empty;

	    if (orbiter != null && orbiter.target != null)
	    {
	        if (orbiter.target.IsDestroyed)
	        {
	            text.text = "<color=red>Destroyed!</color>";
	            return;
	        }


	        foreach (var vitalComp in orbiter.target.GetComponentsInChildren<VitalShipComponent>())
            {
                result += vitalComp.name + ": " + vitalComp.Health + "\n";
            }
	    }

	    text.text = result;
	}
}
