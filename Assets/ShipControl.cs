﻿using UnityEngine;
using System.Collections;

public class ShipControl : MonoBehaviour
{
    private Ship controlledShip;
    private DragMouseOrbit orbiter;

    void Start ()
    {
        orbiter = GetComponent<DragMouseOrbit>();
    }
	
	// Update is called once per frame
	void Update () {
        if (orbiter != null && orbiter.target != null)
            controlledShip = orbiter.target;
        else
            return;

	    controlledShip.MiddleEngine.Throttle -= 1f;

        if (Input.GetKey(KeyCode.W))
	    {
	        controlledShip.MiddleEngine.Throttle += 2f;
            controlledShip.LeftEngine.Throttle += 2f;
            controlledShip.RightEngine.Throttle += 2f;
        }

        controlledShip.LeftEngine.Throttle -= 1f;
        controlledShip.RightEngine.Throttle -= 1f;

        if (Input.GetKey(KeyCode.A))
        {
            controlledShip.LeftEngine.Throttle += 2f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            controlledShip.RightEngine.Throttle += 2f;
        }

	    if (Input.GetKey(KeyCode.Space))
	    {
	        controlledShip.ShipRb.angularDrag = 0.3f;
	    }
	    else
	    {
            controlledShip.ShipRb.angularDrag = 0.0f;
        }
    }
}
