﻿using System.Collections;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    public float ttl;

    public void Start()
    {
        StartCoroutine(DestroyMe(ttl));
    }

    public IEnumerator DestroyMe(float ttl)
    {
        yield return new WaitForSeconds(ttl);
        Destroy(gameObject);
    }
}
