﻿using System.Collections;
using UnityEngine;

public class Missile : MonoBehaviour
{
    private Transform target;
    private float homingSensitivity = 0.05f;
    private float speed = 0f;
    private float maxSpeed = 10;
    private Vector3 prevPosition;
    
    public Transform Target
    {
        get { return target; }
        set { target = value.GetComponentInChildren<VitalShipComponent>().transform; }
    }

    private bool flying;

    public float ttl = 40f;

    public GameObject[] ExplosionEffects;

    private Rigidbody rb;
    private LineRenderer lr;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        lr = GetComponent<LineRenderer>();

        StartCoroutine(StartFlightAfter5Secs());
    }

    private IEnumerator StartFlightAfter5Secs()
    {
        yield return new WaitForSeconds(1f);
        flying = true;
    }

    public void Update()
    {
        if (flying)
        {
            rb.isKinematic = true;
            lr.enabled = true;

            if (target != null)
            {
                var relativePos = target.position - transform.position;
                var rotation = Quaternion.LookRotation(relativePos);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, homingSensitivity);
            }
            speed = Mathf.Min(maxSpeed, speed + 0.1f);
            transform.Translate(0, 0, speed*Time.deltaTime, Space.Self);

            lr.SetPosition(0, transform.position - transform.forward * 0.18f);
            lr.SetPosition(1, prevPosition);
        }

        ttl -= Time.deltaTime;
        if (ttl <= 0)
        {
            Destroy(gameObject);
        }

        prevPosition = transform.position - transform.forward * 0.18f;
    }

    public void FixedUpdate()
    {
        if (!flying) speed = rb.velocity.magnitude;
    }

    void OnCollisionEnter(Collision collision)
    {
        var explosion = (GameObject)Instantiate(ExplosionEffects[Random.Range(0, ExplosionEffects.Length)], transform.position - (Vector3.one * 0.1f) + 0.2f * (Random.insideUnitSphere), Quaternion.identity);
        explosion.transform.localScale = Vector3.one * (0.1f + Random.value * 0.3f);
        var comp = collision.transform.GetComponentInChildren<ShipComponent>();
        if (comp != null) comp.Damage(Random.Range(0, 2));
        Destroy(gameObject);
    }
}