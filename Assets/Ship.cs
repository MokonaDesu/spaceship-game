﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ship : CompoundComponent {

    public override void Start()
    {
        base.Start();
        ShipRb = GetComponent<Rigidbody>();
        ShipRb.mass = CompoundMass;

        RightEngine = transform.FindDeepChild("RightEngine").GetComponent<EngineControl>();
        MiddleEngine = transform.FindDeepChild("MiddleEngine").GetComponent<EngineControl>();
        LeftEngine = transform.FindDeepChild("LeftEngine").GetComponent<EngineControl>();
    }

    public EngineControl RightEngine;
    public EngineControl MiddleEngine;
    public EngineControl LeftEngine;
    public Rigidbody ShipRb;

    public bool IsDestroyed { get; set; }

    public void PickRandomTarget(IEnumerable<Ship> possibleTargets)
    {
        var index = Random.Range(0, possibleTargets.Count());
        GetComponentInChildren<BridgeControl>().Target = possibleTargets.ElementAt(index).transform;
    }

    public bool HasTarget()
    {
        var bridge = GetComponentInChildren<BridgeControl>();
        return bridge == null || (bridge.Target != null && !bridge.Target.GetComponent<Ship>().IsDestroyed);
    }
}
