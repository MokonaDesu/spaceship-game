﻿using UnityEngine;
using UnityEngine.UI;

public class TargetStatus : MonoBehaviour
{
    private DragMouseOrbit orbiter;
    private Text text;

    public void Start()
    {
        orbiter = FindObjectOfType<DragMouseOrbit>();
        text = GetComponent<Text>();
    }

	void Update ()
	{
	    var result = string.Empty;

	    if (orbiter != null && orbiter.target != null)
	    {
	        var bridge = orbiter.target.GetComponentInChildren<BridgeControl>();
            if (bridge == null) return;

	        var gunsTarget = bridge.Target;
	        if (gunsTarget == null)
	        {
	            text.text = "No target";
	            return;
	        }

	        var targetShip = gunsTarget.GetComponentInParent<Ship>();
	        if (targetShip == null) return;


            if (targetShip.IsDestroyed)
	        {
	            text.text = "<color=red>Destroyed!</color>";
	            return;
	        }


	        foreach (var vitalComp in targetShip.GetComponentsInChildren<VitalShipComponent>())
            {
                result += vitalComp.name + ": " + vitalComp.Health + "\n";
            }
	    }

	    text.text = result;
	}
}
