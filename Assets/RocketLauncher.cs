﻿using System.Collections;
using UnityEngine;

public class RocketLauncher : ShipComponent
{
    private float maxCd = 15f;

    private BridgeControl bc;
    private Ship ship;

    public GameObject MissilePrefab;

    public void Start()
    {
        bc = GetComponentInParent<Ship>().GetComponentInChildren<BridgeControl>();
        ship = GetComponentInParent<Ship>();
        StartCoroutine(FireMissiles());
    }

    public bool IsTargetShipDestroyed(Transform targetComp)
    {
        if (targetComp == null) return true;

        var targetShip = targetComp.GetComponentInParent<Ship>();
        return targetShip == null || targetShip.IsDestroyed;
    }

    public IEnumerator ShootMissiles(Transform shootAt)
    {
        for (int i = 0; i < 5; i++)
        {
            var missile = (GameObject)Instantiate(MissilePrefab, (transform.position + 0.3f * transform.up - transform.forward * 0.2f * (i - 2)), Quaternion.Euler(-90f, 0f, 0f));
            var missileRb = missile.GetComponent<Rigidbody>();
            missileRb.AddForce(transform.up, ForceMode.VelocityChange);
            var missileComp = missile.GetComponent<Missile>();
            missileComp.Target = shootAt;

            yield return new WaitForSeconds(0.1f);
        }   
    }

    private IEnumerator FireMissiles()
    {
        while (true)
        {
            if (IsDestroyed || bc.IsDestroyed || ship.IsDestroyed)
            {
                StopCoroutine("FireMissiles");
                break;
            }

            if (bc.Target != null || IsTargetShipDestroyed(bc.Target))
            {
                StartCoroutine(ShootMissiles(bc.Target));

            }

            yield return new WaitForSeconds(maxCd);
        }
    }

    public GameObject[] ExplosionEffects;

    protected override IEnumerator RunDestructionFx()
    {
        for (int i = 0; i < 2; i++)
        {
            var explosion = (GameObject)Instantiate(ExplosionEffects[Random.Range(0, ExplosionEffects.Length)], transform.position - (Vector3.one * 0.1f) + 0.2f * (Random.insideUnitSphere), Quaternion.identity);
            explosion.transform.localScale = Vector3.one * (0.1f + Random.value * 0.3f);
            yield return new WaitForSeconds(Random.value * 0.5f);
        }
    }
}
