﻿using UnityEngine;
using System.Collections;

public class Gun : ShipComponent
{
    private float maxCd = 1f;
    private float maxDeviation = 0.1f;

    private BridgeControl bc;
    private LineRenderer lineRenderer;
    private Ship ship;

    public void Start()
    {
        bc = GetComponentInParent<Ship>().GetComponentInChildren<BridgeControl>();
        lineRenderer = GetComponent<LineRenderer>();
        ship = GetComponentInParent<Ship>();
        StartCoroutine(FireGun());
    }

    public bool IsTargetShipDestroyed(Transform targetComp)
    {
        if (targetComp == null) return true;

        var targetShip = targetComp.GetComponentInParent<Ship>();
        return targetShip == null || targetShip.IsDestroyed;
    }

    public void SetLaserColor(Color c)
    {
        lineRenderer.SetColors(Color.white, c);
        lineRenderer.material.color = 0.4f * Color.white + 0.6f * c;
    }

    private IEnumerator FireGun()
    {
        while (true)
        {
            if (IsDestroyed || bc.IsDestroyed || ship.IsDestroyed)
            {
                StopCoroutine("FireGun");
                break;
            }

            if (bc.Target != null || IsTargetShipDestroyed(bc.Target))
            {
                var targetRay = new Ray(transform.position, bc.Target.position - transform.position);
                RaycastHit hit;
                if (Physics.Raycast(targetRay, out hit))
                {
                    if (hit.transform.GetComponentInParent<Ship>() == bc.Target.GetComponentInParent<Ship>())
                    {
                        Shoot();
                    }
                };
            }

            yield return new WaitForSeconds(0.5f + (maxCd - 0.5f) * Random.value);
        }
    }

    private IEnumerator ShowGunFx(Vector3 hitPoint)
    {
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, hitPoint);
        lineRenderer.enabled = true;
        yield return new WaitForSeconds(0.1f);
        lineRenderer.enabled = false;
    }

    private void Shoot()
    {
        var targetRay = new Ray(transform.position, bc.Target.position - transform.position);
        targetRay.direction += new Vector3((Random.value - 0.5f) * maxDeviation, (Random.value - 0.5f) * maxDeviation, (Random.value - 0.5f) * maxDeviation);
        RaycastHit hit;
        if (Physics.Raycast(targetRay, out hit))
        {
            StartCoroutine(ShowGunFx(hit.point));
            int damage = Random.Range(1, 4);
            var hitPart = hit.collider.GetComponentInChildren<ShipComponent>();
            if (hitPart == null)
            {
                hitPart = hit.collider.GetComponentInParent<ShipComponent>();
            }
            if (hitPart != null) hitPart.Damage(damage);
        }
        else
        {
            StartCoroutine(ShowGunFx(targetRay.origin + targetRay.direction * 100f));
        }
    }

    public GameObject[] ExplosionEffects;

    protected override IEnumerator RunDestructionFx()
    {
        for (int i = 0; i < 2; i++)
        {
            var explosion = (GameObject)Instantiate(ExplosionEffects[Random.Range(0, ExplosionEffects.Length)], transform.position - (Vector3.one * 0.1f) + 0.2f * (Random.insideUnitSphere), Quaternion.identity);
            explosion.transform.localScale = Vector3.one * (0.1f + Random.value * 0.3f);
            yield return new WaitForSeconds(Random.value * 0.5f);
        }
    }
}
