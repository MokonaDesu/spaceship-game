﻿public abstract class VitalShipComponent : ShipComponent
{
    public bool ShipDestroyed;

    public override void DestroyShipComponent()
    {
        ShipDestroyed = true;
        var ship = GetComponentInParent<Ship>();
        ship.IsDestroyed = true;
        FindObjectOfType<GameManager>().NotifyShipDestroyed(ship);
        base.DestroyShipComponent();
    }
}
